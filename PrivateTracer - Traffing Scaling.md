# Traffic and Scaling Analysis
This document is meant to trigger discussion on the traffic load of the system under various assumptions. As the resulting data anaysis is refined it will highlight where optimisations are likely needed in the design. 

One intuitive result is that the data shared to the server scales roughly in line with the number of people in the system - and that data then needs to be downloaded by all users. As the population using the system grows then the download network traffic also scales linearly. One potential optimisation will be to partition (shard) how the data is tagged / downloaded (e.g. per province) with a protocol for downloading other data where needed, but the practicality and privacy implications need investigation. 

Note: this analysis focuses on the network traffic. The Bluetooth traffic that happens on a peer to peer basis is not included.

## Traffic Analysis

In this section, we make some assumptions and perform some calculations to how the system performs in the average and worst case scenario.

### Definitions

* _Population (P)_: In The Netherlands, there are more than 17M people. 
* _App Penetration Perc (P%)_: The percentage of the population using the application
* _New Daily Infections (NDI)_: The number of people infected on a daily basis.
* _Average Daily Interactions (ADI)_: The number of interactions on average per person per day
* _Days Interactions Shared (DIS)_: The number of days for which interactions are shared when a person is declared infected
* _Interaction Record Size (IRS)_: The rough size of the data packet related to a particular interaction
* _Data Sent (TDS)_   to server: Each infected person shares a list of their identifiers for the interations they have recorded locally to the server. Note that these are shared in batch form when the user is declared as being infected. 
* _Data Read (TDR)_ from server: each user downloads daily the whole list of infected interactions. 

### Daily Figures

When a user is marked as infected, the information that user needs to share is the following: ADI * DIS * IRS. 

This then needs to be multiplied by the total number of infections to get to the uploaded traffic per day i.e. TDS = NDI * ADI * DIS * IRS.

This data is then read by all users, so TDR = TDS * P * P%

For all cases we use the following assumptions:
* P = 17M
* IRS = 400 bytes
* DIS = 10

### Data Traffic Analysis

For a first pass analysis we use the following parameters:
* P% = 60%
* NDI = 1000
* ADI = 30

This implies that
* TDS = 1000 * 30 * 10 * 0.4 = 120MB i.e. this is the traffic load on the server, from a total of 1000 submission requests.
* TDR = 120MB * 17M * 60% = 1.22 x10^15 bytes = 1.22 PB 

The expectation is that the number of New Daily Infections (NDI) and the average number of daily interactions (ADI) are related i.e. if NDI is high then ADI will remain low as soceity will remain in lockdown.

## Considerations

1. Server data storage: not a concern at all, with a data submission rate of 120MB / day.
2. Server write capacity: not a concern at all, as there will be write requests scaling linerarly with the number of new infected cases. This does scale with the total population size (infected & not infected)
3. Server read capacity / network load: clearly a concern, as this scales lineary with the total population size. 
4. Reducing the size of an interaction record will help all aspects
5. Content Delivery Network (CDN) caching may help reducing server and network load.
6. If mobile phone users perform their daily download using mobile data, it will potentially have a significant impact on their monthly data bundel.

To make the system scalable a number of options exist:
- Move to smaller population groups that share data e.g. split into  multiple geographic areas. So doing, the user can download only the data related to the regions he travelled to, and the network load is lower - but it does imply a protocol to hand over infected data that crosses regions. 
- Cache content to reduce the burden on the central server/network infrastructure.

## To do
* Submission of data from people who are infected, but who continue to have interactions

