# Introduction

This is a working document intended to capture technical design decisions for the implementation. The reader is assumed to have read the project overview from README.md.

In all discussions below, the concept of privacy by design is fundamental - if any  threats are not adequately addressed then please share directly. 

The design below is still iterating - and no doubt will evolve further. 

Finally, this document focuses on one core use case - but should be expandable to cover additional use cases with incremental additions e.g. channels to allow exchanges for disclosed interactions, sharing of (anonymized) data to health authorities, proximity warnings, aggregating multiple factors in determining the risk profile of a person, or even cases outside the urgent need to track infections due to Covid19.

# Architecture
The high level conceptual architecture is shown below. 

![High level aarchitecture](Images/TechDesign-Arch.png "High level archtitecture")

The key components on the client include:
* Local Storage: On device repository for all information related to detected interactions
* Identities & Credential Manager: Responsible for generating and managing the identities used, as well as credentials attached to the identities
* Bluetooth Communication: Monitoring and detection of nearby devices, and facilitates the exchange of information with peer devices
* Proximity Detection: Identifies using available information which interactions should be tracked
* Server communications Manager: Responsible for managing all interactions with cloud based services

The key server side components include:
* Devices Interface: Provides the set of services that devices use to share infected interactions as well as to obtain the latest known set of infected interactions
* Storage: for storing information related to the known set of infected interactions
* Proxy: Interface for supporting additional cloud based interactions e.g. for roaming, providing dashboards using anonymized data etc. 


# Identities & Credential Manager


## PeerDIDs

All identities are based on Decentralized Identifiers (DIDs).

In order to avoid attacks based on correlation, all identities are  ‘pairwise pseudonymous identifiers’ i.e. an identifier is only used within the context (purpose limitation: GDPR Art. 5  & 13)[^1][^2] of a single interaction that is being tracked. 

As such, each device will generate a new identity for each interaction. The identity standard to be used is PeerDID, which is designed for local peer based interactions. 


## Key Management

Each PeerDID includes a new pub-priv key pair. As each device is generating a new identity for each interaction the device needs to have a simple way to generate new pub-priv key pairs, and to then be able to manage these keys.

The approach that is typically used with the digital asset space for this is called “Hierarchical Deterministic Wallets” - as described in the widely used BIP32 standard. 


![HD Key Generation](Images/TechDesign-HDWallet.png "Hierarchy of deterministically generated keys")


The master key is maintained on the device, and the application would use multiple child branches:

*   Generating the keys embedded within each PeerDID for tracking each interaction 
*   Managing credentials related to the owner of the device e.g. health declaration from trusted authorities



# Bluetooth Communication

The application will leverage Bluetooth to be able to identify people who have been in close proximity as per issued guidelines. This could for example be that another device is within 2m for more than 2 minutes. It includes clearly also people that are in close proximity but with whom there may not have been a direct interaction - which is still an infection risk. 

Absolutely critical is that the detection of the interaction does not require any action on behalf of the user. Once the user installs the app on their phone and enables bluetooth then from that point on the detection happens in the background. 

The first integration tests were carried out with a commercial package (p2pkit.io) for the bluetooth communications - but this is being actively swapped out due to clear data leakages from that library. The team is currently evaluating several alternatives:
*   Leverage the solution from TraceTogether, which the Singaporean government has committed to open source
*   Developer a custom solution directly on OS apis
The team is open to other suggestions. 

It is clear that the target functionality can be achieved using Bluetooth, and in a privacy perserving manner - as demonstrated by TraceTogether. 

The Bluetooth technology version 4.0 will be used in this concept. Bluetooth Low Energy (Bluetooth LE, colloquially BLE, formerly marketed as Bluetooth Smart) is a wireless personal area network technology designed and marketed by the Bluetooth Special Interest Group (Bluetooth SIG) aimed at novel applications in the healthcare, fitness, beacons, security, and home entertainment industries. Compared to Classic Bluetooth, Bluetooth Low Energy is intended to provide considerably reduced power consumption and cost while maintaining a similar communication range. Mobile operating systems including **iOS, Android**, Windows Phone and BlackBerry, as well as macOS, Linux, Windows 8 and Windows 10, natively support Bluetooth Low Energy.

# Proximity Detection

As part of the interaction, information related to characteristis of the signal of the other device will be captured. This information then needs to be calibrated to ensure that it can be translated into actual information about the interaction e.g. how far away the other device is. 

This is a topic for further analysis. If you are aware of such calibration data sets then please get in contact.  


# Local storage

All data related to interactions is stored locally on the device.

For each interaction, a device generates a new PeerDID and uses that to then generate a unique identifier for the interaction per device (i.e. pairwise unique identifiers). These identifiers are called Interaction Identifiers (_IID_). All information related to the interaction is stored under this identifier. 

Over time each device will thus have a sequence of records of interactions that it has encountered.


![Interactions stored on device](Images/TechDesign-Interactions.png "Storage of interactions on devices")


Taking the example shown above, device A will generate a new DID / identifier as will device B - all local to each device. Zooming in to the information relevant to the interaction between device A and device B:

![Detailed view of interaction as stored on device](Images/TechDesign-InteractionDetailView.png "Close up view of a pair of interaction records for a single interaction")

The IID is generated using the public key from the PeerDID, by taking the hash of it and then combining it with additional data e.g. a country / region identifier. 

## Tracking

For each interaction, a device stores the following

*   The local IID (as shared with the peer device), as well as the PeerDID from which it was generated
*   The remote IID (of the peer device), representing the interaction from the perspective of the local device
*   MetaData - additional information about the interaction e.g. date, location, etc
*   PeerData - an encrypted package of information received from the other device 

The meta data that is stored has be chosen carefully to avoid privacy leakages e.g. by storing exact time / location data a user could potentially identify the other infected person. This can be mitigated by storing reduced resolution information e.g. time period / rough location from the interaction. It is an open topic about how to balance this with the expected need to be able to enable the user to opt-in for sharing anonymized interaction data for macro level analysis. 

The package of information is encrypted by the peer device - so that the remote device can later share the corresponding private key to allow the local device to unlock it. For simplicity in the diagrams it is shown as using the pub/priv key from the PeerDID but for production deployment package encryption will be done using a one time key derived from the PeerDID keys.

![Populated interaction record as stored on device](Images/TechDesign-InteractionPopulated.png "Storage of populated interaction on devices")

The result is that each client device will have captured a sequence of interactions, each identified the IID generated by the remote devices it has interacted with. 

![Populated multiple interaction records as stored on devices](Images/TechDesign-Interactions2.png "Storage of populated interactions on devices")


The set of information stored in the encrypted package is an open design choice, users of the app could be offered the option to share personal information(GDPR Art. 13: 2c)[^3] (e.g. name, email etc) and also include a credential to authorise the other person to have that information - thereby ensuring GDPR compliance.


# Notifications

It is critical that once a person is identified as being infected, or even as having a high probability of being infected, that all interactions related to that device can be marked as “infected interactions” and that devices can identify whether they have participated in such interactions.

In the case of TraceTogether, this role is fulfilled by the government. In our case the goal is to design an efficient and privacy preserving approach to sharing the notifications. 

The currently envisaged approach is a two step process:
*   If a user is infected, then the all interactions related to that user should be marked as infected and uploaded from the device to a public location
*   All other devices regularly poll the user for the latest list of infected IIDs and compares that list to the IIDs known locally. If any of them match then the user of the device is notified that they have been in close proximity to someone who is infected and are advised as to further steps to take. 

Note that as every interaction has a unique identifier for each participant, and that as the matching of interactions happens locally there is no correlation analysis possible.


### Publish “Infected Interactions”

When a person has been infected with the virus, then they need to notify all other devices with which they have been in close proximity to. Those interactions are deemed to be "infected interactions".

The publishing is to a “public known web location”, which can initially be a simple website providing a set of posting / lookup services, or a distributed storage solution such as IPFS.

The illustrated example of device A notifying device B that their pairwise interaction is “infected” would look as follows:

![Publish the list of infected interactions](Images/TechDesign-PublishInfectedInteraction.png "Publishing the list of infected interactions")

In addition to publishing the IID, Device A can also optionally publish the private key to enable Device B to unlock the data package stored locally on Device B. As an additional security measure the private key would be encrypted with the public key from pairwise identity on Devise B so that only Device B could access it. Note that only Device B would have the encrypted package so the key should anyway only be relevant to Device B.

The above process is then repeated for every relevant recorded interaction on the device.

The fact a device is publishing a list of IIDs that are marked as infectious will need to be hidden. There are various approaches to be considered for this e.g. layered routing, plausible deniability by publishing regularly fake interactions that are not marked as infectious etc. 


### Download List of Infected Interactions

The second step is for device B to query the repository to obtain the currently known list of “infected interactions”. 


![Download list of infected interactions](Images/TechDesign-DownloadInfectedInteractions.png "Download list of infected interactions")


Devices will periodically query the server to obtain the latest list of infected interactions. It is assumed that the notifications do not need to happen in near real time i.e. it is sufficient to be detect within an hour of another person sharing an update to their profile. It is a topic for further discussion what the correct response time should be.

To avoid repeatedly downloading all the information, the server should order all the interactions and essentially assign each an index. Then when a device requests the latest information on infected interactions it can simply supply with that request the index for the interactions which it has previously downloaded.

Note: as this approach scales to more countries, the design for tracking and requesting the list of known infected interactions will need to be made more sophisticated. Topics to explore include:
* Enabling "roaming" of interactions, whereby devices can also check on the status of IIDs with which they have been in close proximity to but whom originated outside NL
* Using a region identifier embedded in the IID to both reduce the network traffic load and to facilitate "roaming"
* Using bloom filters to avoid all users having to download all the list of infected IIDs


### Locally Match for Infected Interactions

Device B can use the downloaded list of Infected IIDs to detect if any of its locally stored IIDs are on the list.

![Match infected interactions locally](Images/TechDesign-MatchInfectedInteraction.png "Match infected interactions locally")



### Decrypt Locally Stored Packages

As part of the publishing process, Device A can also choose to publish the private key (encrypted with the public key for the interaction from Device B) that enables Device B to unlock the package of information stored locally for that interaction.


![Unlock storage record using key stored from infected interaction record](Images/TechDesign-UnlockStorage.png "Unlock storage record using key stored from infected interaction record")



# Messaging 

The ability to facilitate additional communication between the interacting devices enables a richer set of use cases to be address. One clear example is that it allows a device to share an initial warning that they are likely infected (symptoms based) and then later share a certified credential regarding the infection. The messages are sharable in both directions. 

As each interaction has an identity for each peer device, it is possible to use a simple messaging list to share encrypted messages between the devices.  


![Exchange messages](Images/TechDesign-Messaging1.png "Publishing encrypted messages for peer device")


As the set of messages being shared grows, there can be additional options explored to facilitate the private messaging. 


# 


# Extensions

Basing the design on formal self sovereign identity standards (i.e. PeerDIDs) also enables additional use cases to be supported. One active topic is how to manage the trust in the diagnosis of infection - this clearly has to be coupled to trusted sources of information, and can for example use publised standards such as Verifiable Credentials. 


## Credentials

The Verifiable Credentials standard would allow a device to engage via public ledgers to obtain credentials from third parties that interacting devices can then also verify via the public ledgers. 

These credentials could include the following:



*   Virus confirmation
*   Attributes related to the device owner such as age group (e.g. 40+), province (e.g. Zuid Holland). 
*   …

Note that there are also options worth exploring related to the usage of Zero Knowledge Proofs for the sharing of information between devices. 




## Standards

The following standards are relevant for this design:



*   W3C DID for Main DID and Disposable DID: [https://www.w3.org/TR/did-core/](https://www.w3.org/TR/did-core/)
*   W3C Document 04 January 2020 DID: [https://openssi.github.io/peer-did-method-spec/](https://openssi.github.io/peer-did-method-spec/)
*   W3C Verifiable Credential for health messages and usage consent (purpose limitation-GDPR): https://www.w3.org/TR/vc-data-model/
*   BIP32 for Hierarchical Deterministic Wallets for generation of Disposable ID DIDs: [https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki)
*   

More to be added.


## References



*   TraceTogether - [ttps://www.gov.sg/article/help-speed-up-contact-tracing-with-tracetogether](https://www.gov.sg/article/help-speed-up-contact-tracing-with-tracetogether)
*   BlueTrace - [www.bluetrace.io](www.bluetrace.io)
*   
*   …

Research Publications



*   Contact Tracing Mobile Apps for COVID-19: Privacy Considerations and Related Trade-offs ([https://arxiv.org/pdf/2003.11511.pdf](https://arxiv.org/pdf/2003.11511.pdf))
*   Quantifying dynamics of SARS-CoV-2 transmission suggests that epidemic control and avoidance is feasible through instantaneous digital contact tracing [https://www.medrxiv.org/content/10.1101/2020.03.08.20032946v1](https://www.medrxiv.org/content/10.1101/2020.03.08.20032946v1)

<!-- Footnotes themselves at the bottom. -->

[^1]:
     [https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm](https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm)

[^2]:
     [https://www.privacy-regulation.eu/en/article-5-principles-relating-to-processing-of-personal-data-GDPR.htm](https://www.privacy-regulation.eu/en/article-5-principles-relating-to-processing-of-personal-data-GDPR.htm)

[^3]:
     [https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm](https://www.privacy-regulation.eu/en/article-13-information-to-be-provided-where-personal-data-are-collected-from-the-data-subject-GDPR.htm)