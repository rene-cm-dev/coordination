# Resources

## App client designs
The latest app designs can be found in [Adobe XD](https://xd.adobe.com/view/8f012c9d-68db-440f-50af-b1c363146220-f930/grid)
These designs include all necessary resources: images, color-codes etc.
These can be found at the`specs` button on the top-right

*Note: `specs` is only accessible when viewing a single screenshot, not in the design overview!*

